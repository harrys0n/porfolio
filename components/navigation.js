import styles from "./navigation.module.css";
import Link from "next/link";

export default function Navigation() {
  return (
    <nav id="navigation" className={styles.nav}>
      <div className={styles.nav__separator}></div>
      <ul id="menu" className={styles.nav__list}>
        <li className={styles.nav__listItem}>
          <Link href="/" className={styles.nav__link}>
            Home
          </Link>
        </li>
        <li className={styles.nav__listItem}>
          <Link href="/about" className={styles.nav__link}>
            About
          </Link>
        </li>
        <li className={styles.nav__listItem}>
          <Link href="/work" className={styles.nav__link}>
            Work
          </Link>
        </li>
        <li className={styles.nav__listItem}>
          <Link href="/blog" className={styles.nav__link}>
            Blog
          </Link>
        </li>
      </ul>
      <div className={styles.nav__separator}></div>
    </nav>
  );
}
