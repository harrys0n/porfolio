import styles from "./footer.module.css";
import FooterImage from "./footerImage";

export default function Footer() {
  return (
    <footer className={styles.footer}>
      <FooterImage />
      <ul className={styles.footer__list}>
        <li className={styles.footer__listItem}>
          <a
            className={styles.footer__listLink}
            href="mailto:hello@harryson.xyz"
          >
            Email
          </a>
        </li>
        <li className={styles.footer__listItem}>
          <a
            className={styles.footer__listLink}
            href="/images/Harryson-Resume.pdf"
          >
            Resume
          </a>
        </li>
        <li className={styles.footer__listItem}>
          <a
            className={styles.footer__listLink}
            href="https://www.linkedin.com/in/harryson-ux/"
          >
            LinkedIn
          </a>
        </li>
      </ul>
      <p className={styles.footer__copyright}>
        <small>
          © {new Date().getFullYear()} | Built with <strong>Next.js</strong>.
          Hosted on <strong>Vercel</strong>.
        </small>
      </p>
    </footer>
  );
}
