import styles from "./footerImage.module.css";

export default function FooterImage() {
  let imageOfThePage = pickImage();
  let linkUrl = imageOfThePage.link;
  let src = imageOfThePage.src;
  let alt = imageOfThePage.alt;

  function pickImage() {
    let images = [
      {
        src: "/vectors/undraw_refresh.svg",
        alt: "image of an animal",
      },
      {
        src: "/vectors/undraw_startman.svg",
        alt: "image of a planet",
      },
    ];

    let n = Math.floor(Math.random() * images.length);
    return images[n];
  }

  return (
    <div className={styles.footerImageWrapper}>
      <a
        href={linkUrl}
        rel="noopener noreferrer"
        target="_blank"
        className={styles.footerImageLink}
      >
        <img src={src} alt={alt} className={styles.footerImage} />
      </a>
    </div>
  );
}
