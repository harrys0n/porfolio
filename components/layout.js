import Head from "next/head";
import styles from "./layout.module.css";
import utilStyles from "../styles/utils.module.css";
import Link from "next/link";
import Navigation from "./navigation";
import Footer from "./footer";

export const siteTitle = "Harryson - UX Design";

export default function Layout({ children, home }) {
  return (
    <div className={styles.container}>
      <Head>
        <link rel="icon" href="/favicon/favicon.ico" />
        <meta
          name="description"
          content="A portfolio site for Harry Sotomayor, an UX Designer from the caribbean."
        />
        <meta name="og:title" content={siteTitle} />
      </Head>
      <header className={styles.header}>
        <div className="header__logo">
          <Link
            href="/"
            aria-label="Harryson"
            className={styles.header__logoText}
          >
            <h1
              className={`${styles.heading} ${styles.headingHome} ${styles.headingOther}`}
            >
              <img
                src="/images/profile.png"
                className={`${styles.headerHomeImage} ${utilStyles.borderCircle}`}
                alt="Cartoon avatar of Harryson"
              />
              Harryson
            </h1>
          </Link>
        </div>
        <Navigation />
      </header>
      <main>{children}</main>
      <Footer />
    </div>
  );
}
