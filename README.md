# Portfolio / Blog built with Next.JS

This is a Next.js based static site hosted on Vercel.

## Prerequisites
- install nodejs
- install pnpm

## How to develop
- `pnpm install` to install packages
- `pnpm dev` to serve a development version of the site to localhost with hot reloading

## License
This project is licensed under the MIT license.

## Acknowledgments
This project is an extension of the [Next.JS 'learn' tutorial](https://nextjs.org/learn/basics/create-nextjs-app). The template is based on [ Taryn Ewen's portfolio](https://github.com/tarynewens/nextJS-portfolio) and the color scheme used is from [Zeno Rocha's Dracula](https://draculatheme.com/about). Illustrations were provided by [unDraw](https://undraw.co/). Icons are from [Icons8](https://icons8.com).
