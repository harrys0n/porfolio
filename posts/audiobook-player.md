---
title: "Play, Pause & Read"
date: "2021-05-10"
---

Mockups I made for the Hype4 UI Course using the Eight Red Square Method.


Splashscreen
![mockup](/images/Mockup/audiobook-mockup1.png)


Color Scheme and Fonts
![mockup](/images/Mockup/audiobook-mockup2.png)


Login
![mockup](/images/Mockup/audiobook-mockup3.png)


Favorites
![mockup](/images/Mockup/audiobook-mockup4.png)


Modal
![mockup](/images/Mockup/audiobook-mockup5.png)


History
![mockup](/images/Mockup/audiobook-mockup6.png)


The rest of the mockup is available in [Figma](https://www.figma.com/file/u31mIxHyo4UNpnKCBry8ns/Audiobook-player-Mockup?node-id=0%3A1) with the first and second iteraction side by side.
