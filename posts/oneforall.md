---
title: "One for All. All for One"
date: "2020-09-15"
---

In 2020 I decided to buy the whole Affinity Suite. The subscription fatigue forced me to look for an Adobe Suite replacement. Luckily, Serif launched a price discount a the time for their software. I have used photoshop before. Doing the transition to Affinity Photo was painless for me.

However, I've never used Illustrator before. Serif made their own copy of that program in Affinity Designer. I made the following flat characters as way to test Desginer capabilities.

Even my own logo in the header of this page was made in Affinity Designer. Some critics says that Affinity Designer doesn't have every feature found in Adobe Illustrator. While that might be truth, it doesn't matter. Adobe Suite has a thing called feature creep. That is when your program has many features from other programs that overlap one or another.

It is one of the reasons, apart from the subscription model, that I dislike Adobe. Photoshop, Illustrator and even Publisher have mostly the same features. One could argue that Adobe should only sell Photoshop with every other feature if the difference is really minimal. However it makes perfect business sense to keep people spending money on more programs from your company even if they are mostly the same. Just ask Jetbrains. They have an IDE for almost every popular language. And their each is based on the code of the original IntelliJ.

For Adobe and Jetbrains, this has proven to be profitable strategy. A customer that wants the photoshop for vector images, can just use Adobe Illustrater. Another that wants IntelliJ, but for C# can buy Rider.

It is the stroke of simple genius. Humans are creatures of habit. Adobe and Jetbrains know this. So they made the same product again customized for doing a little something extra. Serif, the makers of Affinity Photo, Designer and know Publisher, went the other way.

All of their products share the same codebase, but each one is made for differenty things. There isn't much overlap here. However in their own way, they made almost an equivalent to what Jetbrains or Adobe offer.

Let me explain. I can use filters and edit raw images in Affinity Photo. I can't in Affinity Designer. The latter can handle vectors and drawings, while the former can't. The same goes for Affinity Publisher. It has features the other two lacks.

Serif in their own stroke of genius made feature in Publisher called Studio Link. Under the condition you have Affinity Photo and Affinity Desginer installed on the computer, Affinity Publisher can use all their features.
