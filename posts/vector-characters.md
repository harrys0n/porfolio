---
title: "Vector Art Showcase"
date: "2020-10-09"
---

The following images were made in Affinity Suite using the tools available by default.


An Orc made of shapes in Affinity Designer
![orc](/images/Shapes/af-suite-vec.PNG)


An abstract backround made in Affinity Photo
![abstract background](/images/background/af-suite-bg.PNG)


An Ice Cream Pop cartoon made of shapes in Affinity Designer and Photo
![ice cream](/images/background/icecream-phonebg.png)


A sunset wallpaper made of strokes and shapes in Affinity Photo
![sunset wallpaper](/images/background/sunsetbg.png)


A background made of stacked isometric text in Affinity Photo
![isometric-background](/images/background/af-suite-wp.PNG)


A knight made of shapes in Affinty Designer
![knight](/images/Shapes/vectorknight.png)
