---
title: "Making a manga app"
date: "2021-03-24"
---

![mockup](/images/Mockup/Library.png)

**The problem:** Anime fans and geek want to read new content not available physically in stores.

**The goal:** Design a manga reader app that allows users to easily select and bookmar japanese comics.

[![diagram](/images/Diagram/manga-af-dg.png)](https://jamboard.google.com/d/1wWKaRUtbS__5KbL8XQpofy9QkGRCzBRA14xY8uwknc0/edit?usp=sharing)



**Research:** I conducted questionares and surveys. Created empathy maps to understand the users needs. A group identified through the research was young adults between 18-24 that are into anime..

This user group confirmed initial assumptions for the Ramen app, but it also revealed that people from other fandoms like american superheroe comics are into japanese comics.

- Users are in need of recomendations
- Users want something simple to use
- Users want more content not available in other mediums



**Wireframes**

As the initial design phase continued, I made sure to base the [screen](https://www.figma.com/file/6Mc4cMm0VLFa9fMtUidQpF/Wireframe%2FMockup?node-id=0%3A1) mimicking other apps that offer similar features.

![wireframe](/images/Wireframe/Followup.png)



**Low-Fidelity Prototype**

The low-fidelity prototype that connects the primary user flow of selecting a japanese comic in the app and switching between sections.

[![wireframe](/images/Wireframe/Wireframe_Mockup.png)](https://www.figma.com/proto/6Mc4cMm0VLFa9fMtUidQpF/Wireframe%2FMockup?node-id=0%3A1&scaling=scale-down&page-id=0%3A1)



**Usability Study**

I conducted a usability study with the prototype to hep improve the design.

- Users did not know about filtering
- Users found the transition between the selecting manga and reading rough.
- Users got confused between update section and history



**Mockup**

Earlier designs had too many sections. After the usability study, this has been simplify for the user. Now intead of having 5 sections in the main navigation bar, it has been [reduced to three](https://www.figma.com/file/6Mc4cMm0VLFa9fMtUidQpF/Wireframe%2FMockup?node-id=101%3A2).

![mockup](/images/Mockup/Search.png)



**High-fidelity prototype**

The final high-fidelity prototype simlifies navigation. Now it includes more visual feedback in the form of quick intro and highlights.

[![mockup](/images/Mockup/Mockup.png)](https://www.figma.com/proto/6Mc4cMm0VLFa9fMtUidQpF/Wireframe%2FMockup?node-id=101%3A2&scaling=scale-down&page-id=101%3A2)




**key takeaways**

While designing the Ramen manga app, I learn that first ideas are the beginning of a proccess. Getting feedback from the users is the buiding block for making an app’s design.
