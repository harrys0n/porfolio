import Head from "next/head";
import Layout, { siteTitle } from "../components/layout";
import utilStyles from "../styles/utils.module.css";
import Link from "next/link";

export default function Home() {
  return (
    <Layout>
      <Head>
        <title>{siteTitle}: Home</title>
      </Head>
      <section className={utilStyles.headingMd}>
        <p>
          Hi, my name is <strong>Harry!</strong> I’m a Front-end Web Developer and{" "}
          <strong>UI/UX Designer</strong> from PR.
        </p>
        <p>
          I'm passionate about inclusion, accessibility, and improving user
          experiences for all. I also enjoy reading a lot and talking about
          tech.
        </p>

        <div>
          <Link
            href="/about"
            className={`${utilStyles.linkFeature} ${utilStyles.linkFeatureCTA}`}
          >
            Learn more about me →
          </Link>
          <Link
            href="/work"
            className={`${utilStyles.linkFeature} ${utilStyles.linkFeatureCTA}`}
          >
            See my work →
          </Link>
        </div>
      </section>
    </Layout>
  );
}
