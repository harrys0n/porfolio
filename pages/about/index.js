import Head from "next/head";
import Layout, { siteTitle } from "../../components/layout";
import utilStyles from "../../styles/utils.module.css";

export default function About() {
  return (
    <Layout>
      <Head>
        <title>{siteTitle}: About</title>
      </Head>

      <section className={utilStyles.headingMd}>
      <h2>About me</h2>
        <p>
        A developer and designer from the Caribbean with a passion for
        connecting people with technology.
        </p>
        
        <p>
          I'm currently working as a software engineer, specializing in Front-End development. 
        </p>
        </section>

      <section className={utilStyles.imageWrapper}>
        <img
          src="/images/profile2.jpg"
          className={utilStyles.imageBase}
          alt="Harry holding a sign with his initial."
        />
      </section>

      <section className={utilStyles.headingMd}>
        <h2>My career</h2>
        <ul className={utilStyles.listStandard}>
          <li className={utilStyles.listItemStandard}>
            <strong>2015:</strong> Taught myself to write HTML and CSS so I
            could build wordpress sites
          </li>
          <li className={utilStyles.listItemStandard}>
            <strong>2016:</strong> Built a website for medical research about drug abuse and stigma
          </li>
           <li className={utilStyles.listItemStandard}>
            <strong>August 2016:</strong> Worked on desiging an online medical form web application
          </li>
           <li className={utilStyles.listItemStandard}>
            <strong>December 2016:</strong> Built and designed the website for the college student-owned online newspaper
          </li>
           <li className={utilStyles.listItemStandard}>
            <strong>June 2017:</strong> Graduated from IT in the University of Puerto Rico
          </li>
           <li className={utilStyles.listItemStandard}>
            <strong>January 2018:</strong> Worked with Kapok Digital on creating an online chatbot for the Diversity Movement website
          </li>
           <li className={utilStyles.listItemStandard}>
            <strong>August 2019:</strong> Redesign the Live Oak Bank homepage, ensuring it was responsive and visually appealing
          </li>
           <li className={utilStyles.listItemStandard}>
            <strong>June 2021:</strong> Built a user-friendly website with a modern and responsive interface for the local tech startup TechFirst
          </li>
           <li className={utilStyles.listItemStandard}>
            <strong>January 2023:</strong> Joined VacoBuilt team to work on solutions for their clients
          </li>
        </ul>
      </section>

      <section className={utilStyles.headingMd}>
      <h2>My skills</h2>
      <ul className={utilStyles.icoGrid}>
        <li>
        <img
        src= "/vectors/icons8-html-5.svg"
          />
        </li>

        <li>
        <img
        src= "/vectors/icons8-css3.svg"
          />
        </li>
        
        <li>
        <img
        src= "/vectors/icons8-javascript.svg"
          />
        </li>
        
        <li>
        <img
        src= "/vectors/icons8-figma.svg"
          />
        </li>

        <li>
        <img
        src= "/vectors/icons8-bootstrap.svg"
          />
        </li>

        <li>
        <img
        src= "/vectors/icons8-sass-avatar.svg"
          />
        </li>

        <li>
        <img
        src= "/vectors/icons8-react-native.svg"
          />
        </li>
        
        <li>
        <img
        src= "/vectors/icons8-git.svg"
          />
        </li>
       
      </ul>
      </section>
    </Layout>
  );
}
