import Head from "next/head";
import Layout, { siteTitle } from "../../components/layout";
import utilStyles from "../../styles/utils.module.css";

export default function Work() {
  return (
    <Layout>
      <Head>
        <title>{siteTitle}: Work</title>
      </Head>
      <section className={utilStyles.headingMd}>
        <h2>My work</h2>
      </section>

      <ul className={utilStyles.grid}>
        <li className={utilStyles.card}>
          <a href="./posts/making-a-manga">
            <img src="/images/Mockup/Mockup.png" alt="manga app" />
            <h3>Ra-mn</h3>
          </a>
        </li>

        <li className={utilStyles.card}>
          <a href="./posts/audiobook-player">
            <img src="/images/Mockup/bookplay.png" alt="Audiobook" />
            <h3>Bookplay</h3>
          </a>
        </li>
      </ul>
    </Layout>
  );
}
